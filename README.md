# Quinb

Quinb is a multiplayer reaction game: train your mind and your reflexes while having fun!

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="70">](https://play.google.com/store/apps/details?id=xyz.deepdaikon.quinb)
[<img src="https://static.itch.io/images/badge.svg"
     alt="Get it on itch.io"
     height="45">](https://deepdaikon.itch.io/quinb)
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="70">](https://f-droid.org/packages/xyz.deepdaikon.quinb/)

## About

[![Liberapay](https://img.shields.io/liberapay/patrons/deepdaikon.svg?logo=liberapay)](https://liberapay.com/deepdaikon/donate)
[<img src="https://www.ko-fi.com/img/githubbutton_sm.svg" alt="Ko-fi" height="20">](https://ko-fi.com/deepdaikon)
[<img src="https://translate.deepdaikon.xyz/widgets/quinb/-/svg-badge.svg" alt="Translation status" height="20">](https://translate.deepdaikon.xyz/engage/quinb/)

Quinb is a reaction / logic game for up to 4 players on the same device.

It contains many minigames in which you have to answer questions as fast as you can to get a point.
If the answer is correct you score a point, otherwise you lose one.

These games are based on **3 different categories**:

* **Logic**: games that require intuition, logic and fast reflexes
* **Audio**: sound-based games, you have to listen carefully to find out the correct answer
* **Vibration**: vibration-based games that require you to listen carefully to the vibrations of your device

Each match consists of a succession of different minigames.
The goal is to score 7 points before your opponents.

You can play alone if you want, but it is more fun to play with friends of all ages. It's great if you are stuck with nothing to do while you're with friends.

If you think you're fast, challenge and beat your friends!

**Main features:**

* 28+ minigames
* Up to 4 players on the same device
* Completely free
* No Ads
* Multiple languages
* Minimalist design

## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/HomePage.png" height="320">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/Screen_1.png" height="320">
<img src ="fastlane/metadata/android/en-US/images/phoneScreenshots/Screen_2.png" height="320">

## Donate

If you want to support the development of Quinb you can donate through Liberapay or Ko-fi.

[<img src="https://liberapay.com/assets/widgets/donate.svg" alt="Donate" height="30">](https://liberapay.com/deepdaikon/donate)
[<img src="https://cdn.ko-fi.com/cdn/kofi3.png?v=2" alt="Donate" height="30">](https://ko-fi.com/deepdaikon)

## Translate

You can use [Weblate](https://translate.deepdaikon.xyz/engage/quinb/) if you want to help with the translation of Quinb.
If your language is missing click on the ["Start new translation"](https://translate.deepdaikon.xyz/projects/quinb/ui/) button.

[<img src="https://translate.deepdaikon.xyz/widgets/quinb/-/multi-auto.svg" alt="Translation status">](https://translate.deepdaikon.xyz/engage/quinb/)

Keep in mind that you need to be signed in Weblate to translate Quinb. Your username and email address will be placed in the translated files to keep track of the co-authors of the translations.

You can make anonymous suggestions if you don't want to sign in.

Thank you :)

## What people say

Some of the reviews and comments received:

- "Very entertaining for multiple people :)" - J.

- "Very well built app. Taught my little niece how to play, now we can have fun and learn !! Good work" - M.

- "Love the game and can't wait for the next update :)" - R.

- "You did a very nice game!" - T.B.

- "Installed from play store only to add 5 stars. Returned to fdroid version" - D.Q.

## Build

0. Clone this project (`git clone https://gitlab.com/DeepDaikon/Quinb.git`)

1. To build this app you need the Flutter SDK and the Android SDK. If you already have them go to step 2.

The official installation guide about this can be found here: https://flutter.dev/docs/get-started/install. It is well explained but if you have problems open an issue so that I can help you.

You can check that everything is fine by running `flutter doctor`.

2. Run `flutter version x.y.z` (replace x.y.z with the flutter version specified in the [`pubspec.yaml` file](https://gitlab.com/DeepDaikon/Quinb/-/blob/master/pubspec.yaml#L7). E.g. `flutter version 2.10.3`).

Run `flutter --version` to check that you are on the right version.

3. Open the Quinb directory and build the app by running `flutter build apk`

Generated apks are placed in `Quinb/build/app/outputs/apk`.

If you have Quinb on your device installed from F-Droid, you have to uninstall it before installing the new apk.

If you want to test your code or your translations you can use `flutter run` which allows you to reload the code of a live running app without restarting it. Use flutter run and then hot reload by pressing 'r'. If you have configured an IDE like Android Studio you can use the hot reload by clicking on its icon.
