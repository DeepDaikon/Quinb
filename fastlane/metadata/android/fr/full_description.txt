Quinb est un jeu de réflexe et de logique jusqu'à 4 joueurs sur le même appareil.

Il contient beaucoup de minijeux où on doit répondre à des questions le plus vite possible pour marquer des points.
If the answer is correct you score a point, otherwise you lose one.

Ces jeux sont basés sur <b>3 catégories différentes</b> :

‣ <b>Logique</b> : jeux qui requièrent de l'intuition, de la logique et de bons réflexes
‣ <b>Audio</b> : jeux sonores, vous devez bien écouter pour trouver la bonne réponse
‣ <b>Vibration</b> : jeux basés sur la vibration qui vous demandent de bien écouter les vibrations de votre appareil

Each match consists of a succession of different minigames.
The goal is to score 7 points before your opponents.

On peut jouer seul si on veut, mais c'est plus amusant de jouer avec des amis de tous âges. C'est génial si on est coincé quelque part sans rien à faire avec des amis.

Si vous pensez être rapide, défiez vos amis !

<b>Main features:</b>

★ 28+ minigames
★ Up to 4 players on the same device
★ Completely free
★ No Ads
★ Multiple languages
★ Minimalist design
