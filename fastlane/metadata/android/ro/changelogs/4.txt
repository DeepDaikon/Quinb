* Adăugă opțiunea pentru a dezactiva jocurile
* Actualizează traducerile
* Fixează emoji pe dispozitivele vechi
* Corectează erori
