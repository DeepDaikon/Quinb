Quinb và một tựa game phản ứng / logic dành cho tối đa 4 người chơi trên một thiết bị.

Nó chứa nhiều minigame trong đó bạn phải trả lời các câu hỏi nhanh nhất có thể để nhận một điểm.
If the answer is correct you score a point, otherwise you lose one.

Các trò chơi này được dựa trên 3 thể loại khác nhau:

‣ <b>Logic</b>: các trò chơi yêu cầu phán đoán, logic và phản xạ nhanh
‣ <b>Âm thanh</b>: các trò chơi dựa trên âm thanh, bạn phải lắng nghe kỹ để tìm ra câu trả lời đúng
‣ <b>Rung</b>: các trò chơi dựa trên những lần rung yêu cầu bạn phải lắng nghe kỹ các lần rung của thiết bị

Each match consists of a succession of different minigames.
The goal is to score 7 points before your opponents.

Bạn có thể chơi một mình nếu bạn muốn, nhưng chơi với các bạn ở mọi lứa tuổi thì sẽ vui hơn. Nó rất tuyệt vời nếu bạn không có gì để làm trong khi bạn đang ở cùng bạn bè.

Nếu bạn nghĩ là bạn nhanh, hãy thách thức và đánh bại bạn bè của bạn!

<b>Main features:</b>

★ 28+ minigames
★ Up to 4 players on the same device
★ Completely free
★ No Ads
★ Multiple languages
★ Minimalist design
