import 'dart:convert';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/services.dart';
import 'package:quinb/game/game.dart';

/// AudioPlayer. Can play one audio at a time.
final AudioPlayer _audioPlayer = AudioPlayer()
  ..setPlayerMode(PlayerMode.lowLatency)
  ..setReleaseMode(ReleaseMode.stop)
  ..audioCache.prefix = 'assets/audio/';

/// Add audio capabilities to Game
mixin Audio on Game {
  /// List of available sounds (in assets/audio/)
  static late List<String> soundList;
  static Future<void> loadSoundList() async {
    final Map<String, dynamic> manifestMap =
        json.decode(await rootBundle.loadString('AssetManifest.json'));
    soundList = manifestMap.keys
        .where((String key) => key.contains('.wav'))
        .map((e) => e.split('/').last)
        .toList();
    _audioPlayer.audioCache.loadAll(soundList);
  }

  /// Play an asset sound
  Future<void> playSound(String fileName) =>
      _audioPlayer.stop().then((_) => _audioPlayer.play(AssetSource(fileName)));
}
