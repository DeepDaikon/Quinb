import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/audio.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';

/// Tap the screen if what you see is what you hear
class AnimalCoherenceGame extends Game with Audio {
  AnimalCoherenceGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  final int time = 1300 + Game.invertedDifficultyIndex * 200;

  /// Animals used
  late Animal textAnimal;
  late Animal soundAnimal;
  late Animal emojiAnimal;
  late int quarterTurns;

  @override
  void update() {
    textAnimal = Animal.values[Random().nextInt(Animal.values.length)];
    soundAnimal = Animal.values[Random().nextInt(Animal.values.length)];
    emojiAnimal = Animal.values[Random().nextInt(Animal.values.length)];
    quarterTurns = multiplayer ? Random().nextInt(3) : 0;
    correctValue = (textAnimal == soundAnimal && textAnimal == emojiAnimal)
        ? Option.yes.value
        : 0;
    playSound(soundAnimal.fileName);
  }

  @override
  Widget widget() => questionWidget(
        child: Column(
          children: <Widget>[
            if (multiplayer) innerWidget(rotated: true),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 6),
              child: RotatedBox(
                quarterTurns: quarterTurns,
                child: Center(
                  child: Text(
                    emojiAnimal.emoji,
                    style: TextStyle(
                        fontFamily: 'emoji', fontSize: innerWidgetHeight / 2.5),
                  ),
                ),
              ),
            ),
            innerWidget(),
          ],
        ),
      );

  Widget innerWidget({bool rotated = false}) => Expanded(
        child: FittedBox(
          fit: BoxFit.fitHeight,
          child: RotatedBox(
              quarterTurns: rotated ? 2 : 0, child: Text(textAnimal.name.i18n)),
        ),
      );
}
