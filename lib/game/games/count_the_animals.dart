import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/object.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Count the animals
class CountTheAnimalsGame extends Game {
  CountTheAnimalsGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  final int time = 3000 + Game.invertedDifficultyIndex * 200;

  @override
  String get question => _question;
  late String _question;

  /// Number of emoji displayed
  late int itemCount;
  late Map<String, int> counter;

  /// List of objects
  late List<GameObject> objectList;

  @override
  void update() {
    options.clear();
    itemCount = 1 + settings.difficulty.index + Random().nextInt(3);
    counter = {for (var a in Animal.values) a.emoji: 0};
    objectList = List<GameObject>.generate(itemCount, (_) {
      var animal = Animal.values[Random().nextInt(Animal.values.length)].emoji;
      counter[animal] = counter[animal]! + 1;
      return GameObject(text: animal);
    });
    switch (Random().nextInt(4)) {
      case 0:
        _question = 'What is the most frequent animal?'.i18n;
        for (final animal in Animal.values) {
          options.add(Option(Animal.values.indexOf(animal),
              text: animal.name.i18n.capitalize));
        }
        var quantity = 0;
        counter.forEach((key, value) {
          if (value > quantity) {
            quantity = value;
            correctValue = Animal.values.indexWhere((e) => e.emoji == key);
          } else if (value == quantity) {
            correctValue = -1;
          }
        });
        break;
      case 1:
        var animal = Animal.values[Random().nextInt(Animal.values.length)];
        _question = 'How many %s are there?'.i18n.fill([animal.emoji]);
        correctValue = counter[animal.emoji];
        options = [
          correctValue!,
          for (var i in <int>[1, 2, 3])
            correctValue! +
                (correctValue! - i >= 0 && Random().nextBool() ? -i : i)
        ].toOptions()
          ..shuffle();
        break;
      case 2:
        var animalOne = Animal.values[Random().nextInt(Animal.values.length)];
        var animalTwo = Animal.values
            .where((a) => a != animalOne)
            .toList()[Random().nextInt(Animal.values.length - 1)];
        _question = 'Are there more %s than %s?'
            .i18n
            .fill([animalOne.emoji, animalTwo.emoji]);
        options = [Option.yes];
        correctValue = counter[animalOne.emoji]! > counter[animalTwo.emoji]!
            ? Option.tap.value
            : 0;
        break;
      case 3:
        var animal = Animal.values[Random().nextInt(Animal.values.length)];
        var count = Random().nextBool()
            ? max(0,
                (counter[animal.emoji] as int) + (Random().nextBool() ? 1 : -1))
            : counter[animal.emoji];
        _question = count == 1
            ? 'Is there only one %s?'.i18n.fill([animal.emoji])
            : 'Are there %s %s?'.i18n.fill([count!, animal.emoji]);
        options = [Option.yes];
        correctValue = count == counter[animal.emoji] ? Option.tap.value : 0;
        break;
    }
  }

  @override
  Widget widget() => questionWidget(
        hasEmoji: true,
        child: isMainPhase
            ? Container(
                alignment: AlignmentDirectional.topStart,
                child: Stack(
                  children: [
                    for (var o in objectList)
                      Positioned(
                        left: Random()
                            .nextInt(
                              (MediaQuery.of(ui.context).size.width / 1.2)
                                  .floor(),
                            )
                            .toDouble(),
                        top: Random()
                            .nextInt((innerWidgetHeight / 2).floor())
                            .toDouble(),
                        child: RotatedBox(
                          quarterTurns: o.quarterTurns,
                          child: Text(
                            o.text,
                            style: const TextStyle(
                                fontSize: 34, fontFamily: 'emoji'),
                          ),
                        ),
                      ),
                  ],
                ),
              )
            : Padding(
                padding: const EdgeInsets.all(30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    if (multiplayer)
                      Expanded(
                        child: FittedBox(
                          fit: BoxFit.fitHeight,
                          child: RotatedBox(
                            quarterTurns: 2,
                            child: Text(
                              counterToString,
                              style: const TextStyle(fontFamily: 'emoji'),
                            ),
                          ),
                        ),
                      ),
                    Expanded(
                      child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: Text(
                          counterToString,
                          style: const TextStyle(fontFamily: 'emoji'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      );

  String get counterToString {
    return counter.entries.fold('', (previousValue, entry) {
      return (previousValue as String) + '${entry.key} = ${entry.value}\n';
    }).trim();
  }
}
