import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Count the lights
class CountTheLightsGame extends Game {
  CountTheLightsGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  /// Total number of lights
  final int lights = 16 + settings.difficulty.index * 4;

  /// Time the lights stay on
  final int timeOn = 300 + (6 - settings.difficulty.index) * 100;

  /// Lights off
  late List<int> lightsOff;

  /// Lights on now
  final List<int> currentLights = [];

  @override
  Future<void> prePhase() async {
    lightsOff = List<int>.generate(lights, (i) => i);
    for (var i = 0;
        i < 4 + Random().nextInt(settings.difficulty.index + 1);
        i++) {
      if (!isPrePhase) return;
      currentLights.clear();
      for (var i = 0;
          i < 1 + Random().nextInt(settings.difficulty.index + 3);
          i++) {
        if (lightsOff.isEmpty) return;
        var light = lightsOff[Random().nextInt(lightsOff.length)];
        currentLights.add(light);
        lightsOff.remove(light);
      }
      await Future.delayed(Duration(milliseconds: timeOn));
      ui.refresh();
    }
    correctValue = lights - lightsOff.length;
    var startIndex = correctValue! - 4 + Random().nextInt(4);
    options = List<int>.generate(4, (i) => startIndex + i + 1).toOptions();
    await Future.delayed(Duration(milliseconds: timeOn));
  }

  @override
  Widget widget() => isPrePhase
      ? GridView.count(
          physics: const NeverScrollableScrollPhysics(),
          childAspectRatio: (MediaQuery.of(ui.context).size.width /
                  (4 + settings.difficulty.index)) /
              (widgetHeight / lights * (4 + settings.difficulty.index)),
          crossAxisCount: 4 + settings.difficulty.index,
          children: List.generate(lights, (index) {
            return DecoratedBox(
              decoration: BoxDecoration(
                color: currentLights.contains(index)
                    ? Colors.orange
                    : Colors.grey.shade800,
                border: Border.all(color: Colors.black),
              ),
            );
          }),
        )
      : super.widget();
}
