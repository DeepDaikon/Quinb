import 'dart:async';
import 'dart:math';

import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:vibration/vibration.dart';

/// Tap after the vibration
class FastFingerGame extends Game {
  FastFingerGame(int players, GamePageState gamePage)
      : super(players, gamePage);
  Timer? _updater;

  @override
  String get question {
    if (isPostPhase) {
      if (correctValue == 0) {
        return 'There was no vibration!'.i18n;
      } else if (stopwatch.isRunning) {
        return "You haven't tapped!".i18n;
      }
      return '+ ${stopwatch.elapsedMilliseconds} ms';
    }
    return 'Tap as soon as you feel a vibration'.i18n;
  }

  @override
  List<Option> options = [Option.tap];

  final Stopwatch stopwatch = Stopwatch();

  @override
  Future<void> mainPhase() async {
    correctValue = 0;
    _updater = Timer.periodic(
        Duration(
            milliseconds: 1500 +
                settings.difficulty.index * 100 +
                Random().nextInt(5) * 500), (_) async {
      if (!isMainPhase || correctValue != 0) return;
      stopwatch.reset();
      stopwatch.start();
      correctValue = Option.tap.value;
      await Vibration.vibrate(duration: 500);
    });
  }

  @override
  bool validateInput(int input) {
    _updater?.cancel();
    stopwatch.stop();
    return super.validateInput(input);
  }

  @override
  void dispose() {
    _updater?.cancel();
    stopwatch.stop();
    super.dispose();
  }
}
