import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/colors.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Find the fastest object
class FastestObjectGame extends Game {
  FastestObjectGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  final int objectCount = 3 + settings.difficulty.index;
  double get objectSize => widgetHeight / 8;

  /// Offset Animation list
  late List<Animation<Offset>> animationList = List<Animation<Offset>>.generate(
      objectCount,
      (_) => Tween<Offset>().animate(AnimationController(vsync: ui)));

  /// Animation controlled that could be used by the game
  late List<AnimationController> animationControllerList =
      List.generate(objectCount, (index) => AnimationController(vsync: ui));

  /// List of objects movements
  late List<Movement> movements;
  List<Movement> get _movementsSorted =>
      List.from(movements)..sort((a, b) => (b.speed - a.speed).round());
  late List<Color> colors;

  @override
  void pause([pauseMenu]) {
    super.pause();
    if (isPaused) {
      for (final ac in animationControllerList) {
        ac.stop(canceled: false);
      }
    } else {
      for (final ac in animationControllerList) {
        if (ac.duration != null) ac.repeat(reverse: true);
      }
    }
  }

  @override
  Future<void> prePhase() async {
    colors = colorList.getRandomSublist(objectCount).cast<Color>().toList();
    movements = List.generate(
        objectCount,
        (i) => Movement(
            colors[i].value,
            MediaQuery.of(ui.context).size.width ~/ objectSize,
            widgetHeight * 0.7 ~/ objectSize));
    options = List.generate(objectCount,
        (index) => Option(colors[index].value, text: colors[index].name));
    calculateAnimations();
    options.removeWhere((element) => [
          if (options.length > 5)
            _movementsSorted[_movementsSorted.length - 1].id,
          if (options.length > 6)
            _movementsSorted[_movementsSorted.length - 2].id
        ].contains(element.value));
    correctValue = _movementsSorted.first.id;
  }

  /// Set animationControllerList values
  void calculateAnimations() {
    for (var i = 0; i < animationControllerList.length; i++) {
      animationControllerList[i].duration = movements[i].duration;
      animationControllerList[i].repeat(reverse: true);
      animationList[i] =
          Tween<Offset>(begin: movements[i].begin, end: movements[i].end)
              .animate(animationControllerList[i]);
    }
  }

  @override
  Widget widget() => questionWidget(
        child: Container(
          alignment: AlignmentDirectional.topStart,
          child: Stack(
            children: <Widget>[
              for (int i = 0; i < animationList.length; i++)
                SlideTransition(
                  position: animationList[i],
                  child: CircleAvatar(
                      backgroundColor: colors[i], radius: objectSize / 2),
                ),
            ],
          ),
        ),
      );

  @override
  void dispose() {
    for (final ac in animationControllerList) {
      ac.dispose();
    }
    animationControllerList.clear();
    super.dispose();
  }
}

/// Object movement used by ObjectSpeedGame
class Movement {
  Movement(this.id, int maxDx, int maxDy) {
    duration = Duration(seconds: 1 + Random().nextInt(3));
    begin = _randomOffset(maxDx, maxDy);
    end = _randomOffset(maxDx, maxDy);
  }

  /// Color value
  final int id;

  /// Begin point
  late Offset begin;

  /// End point
  late Offset end;

  /// Transition duration
  late Duration duration;

  /// Transition distance
  double get distance => (end - begin).distance;

  /// Transition speed
  double get speed => distance / duration.inSeconds;

  Offset _randomOffset(int maxDx, int maxDy) => Offset(
      Random().nextInt(maxDx).toDouble(), Random().nextInt(maxDy).toDouble());
}
