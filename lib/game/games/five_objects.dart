import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/object.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';

/// Tap when there are 5 different objects
class FiveObjectsGame extends Game {
  FiveObjectsGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  final int time = 1000 + Game.invertedDifficultyIndex * 200;

  /// Number of emoji displayed
  static const int itemCount = 6;

  /// List of objects
  final List<GameObject> objectList = [];

  @override
  void update() {
    var emojiSublist = emojis.getRandomSublist(6).cast<String>().toList();
    objectList.clear();
    for (var i = 0; i < itemCount; i++) {
      var emoji = emojiSublist[Random().nextInt(emojiSublist.length)];
      objectList.add(GameObject(
          text: emoji, highlighted: objectList.every((e) => e.text != emoji)));
    }
    correctValue = objectList.toSet().length >= 5 ? Option.tap.value : 0;
  }

  @override
  Widget widget() => questionWidget(
        child: GridView.count(
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: itemCount ~/ 2,
          childAspectRatio:
              (MediaQuery.of(ui.context).size.width / (itemCount ~/ 2)) /
                  (innerWidgetHeight / 2),
          children: List.generate(itemCount, (index) {
            return Container(
              margin: const EdgeInsets.all(4),
              decoration: isPostPhase && objectList[index].highlighted
                  ? BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: correctValue != 0 ? Colors.green : Colors.red,
                        width: 5,
                      ),
                    )
                  : const BoxDecoration(),
              child: Center(
                child: RotatedBox(
                  quarterTurns: objectList[index].quarterTurns,
                  child: Text(
                    objectList[index].text,
                    style: const TextStyle(fontSize: 30, fontFamily: 'emoji'),
                  ),
                ),
              ),
            );
          }),
        ),
      );
}
