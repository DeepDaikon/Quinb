import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Lights on game
class LightsOnGame extends Game {
  LightsOnGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  final int time = 500 + Game.invertedDifficultyIndex * 200;

  /// Total number of lights
  final int lights = 16 + settings.difficulty.index * 4;

  /// Lights on now
  late List<bool> currentLights;

  @override
  void update() {
    currentLights = List.generate(lights, (_) => Random().nextBool());
    correctValue = currentLights.where((l) => l).length > lights / 2
        ? Option.tap.value
        : 0;
  }

  @override
  Widget widget() => questionWidget(
        child: Stack(
          children: [
            GridView.count(
              physics: const NeverScrollableScrollPhysics(),
              childAspectRatio: (MediaQuery.of(ui.context).size.width /
                      (4 + settings.difficulty.index)) /
                  (innerWidgetHeight /
                      lights *
                      (4 + settings.difficulty.index)),
              crossAxisCount: 4 + settings.difficulty.index,
              children: List.generate(
                lights,
                (index) => DecoratedBox(
                  decoration: BoxDecoration(
                    color:
                        currentLights[index] ? Colors.orange : Colors.grey[800],
                    border: Border.all(color: Colors.black),
                  ),
                ),
              ),
            ),
            if (isPostPhase)
              Positioned.fill(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (multiplayer)
                      RotatedBox(
                        quarterTurns: 2,
                        child: Text(percentage,
                            style: const TextStyle(
                                color: Colors.white, fontSize: 40)),
                      ),
                    Text(percentage,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 40)),
                  ],
                ),
              ),
          ],
        ),
      );

  String get percentage => '${currentLights.where((l) => l).length} / $lights';
}
