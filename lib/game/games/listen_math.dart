import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/audio.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:vibration/vibration.dart';

/// Listen and calculate
class ListenMathGame extends Game with Audio {
  ListenMathGame(int players, GamePageState gamePage,
      {this.useVibration = false})
      : super(players, gamePage);

  /// True if outputs are vibrations instead of sounds
  bool useVibration;

  /// Sequence of sounds/vibrations
  late List<bool> sequence;

  /// Sum and Subtraction amount
  /// Sum: duck / long vibration
  /// Sub: cat / short vibration
  late int sumValue;
  late int subValue;

  @override
  Future<void> prePhase() async {
    sequence = List.generate(
        4 + Random().nextInt(2 + settings.difficulty.index),
        (_) => Random().nextBool());
    sumValue = max(1, settings.difficulty.index + Random().nextInt(4));
    subValue = min(-1, -settings.difficulty.index - Random().nextInt(4));
    correctValue = sequence.fold(
        0,
        (previousValue, element) =>
            previousValue! + (element ? sumValue : subValue));
    var startIndex = correctValue! - 4 + Random().nextInt(4);
    options = List<int>.generate(4, (i) => startIndex + i + 1).toOptions();
    for (final isSum in sequence) {
      if (!isPrePhase) return;
      await (useVibration
          ? Vibration.vibrate(duration: isSum ? 300 : 150)
          : playSound(isSum ? Animal.duck.fileName : Animal.cat.fileName));
      await Future.delayed(const Duration(seconds: 1));
    }
  }

  @override
  Widget widget() => isPrePhase
      ? Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            if (multiplayer) ...[
              RotatedBox(quarterTurns: 2, child: innerWidget()),
              const Divider(height: 1),
            ],
            innerWidget(),
          ],
        )
      : super.widget();

  Widget innerWidget() => Container(
        constraints:
            BoxConstraints(maxWidth: MediaQuery.of(ui.context).size.width / 2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(useVibration ? '__' : Animal.duck.emoji,
                    style: TextStyle(
                      fontSize: 32,
                      fontFamily: useVibration ? null : 'emoji',
                    )),
                Text('+$sumValue', style: const TextStyle(fontSize: 32)),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(useVibration ? '- ' : Animal.cat.emoji,
                    style: TextStyle(
                      fontSize: 32,
                      fontFamily: useVibration ? null : 'emoji',
                    )),
                Text(subValue.toString(), style: const TextStyle(fontSize: 32)),
              ],
            ),
          ],
        ),
      );
}
