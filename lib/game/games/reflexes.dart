import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/colors.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Reflexes game
class ReflexesGame extends Game {
  ReflexesGame(int players, GamePageState gamePage) : super(players, gamePage);
  Timer? _updater;

  /// Colors used
  final int colorCount = min(1 + settings.difficulty.index, 4);
  late List<Color> colors;

  @override
  Future<void> mainPhase() async {
    colors = colorList.getRandomSublist(colorCount).cast<Color>().toList();
    correctValue = Colors.white.value;
    options = [for (var i = 0; i < colorCount; i += 1) colors[i].value]
        .toOptions(asColors: true);
    _updater = Timer.periodic(
        Duration(milliseconds: 2000 + (Random().nextInt(5)) * 1000), (_) async {
      if (!isMainPhase) return;
      correctValue = colors[Random().nextInt(colorCount)].value;
      ui.refresh();
      Future.delayed(Duration(
              milliseconds: 400 + (6 - settings.difficulty.index) * 100))
          .then((_) {
        if (!isMainPhase) return;
        correctValue = Colors.white.value;
        ui.refresh();
      });
    });
  }

  @override
  bool validateInput(int input) {
    _updater?.cancel();
    return super.validateInput(input);
  }

  @override
  Widget widget() =>
      questionWidget(child: Container(color: Color(correctValue!)));

  @override
  void dispose() {
    _updater?.cancel();
    super.dispose();
  }
}
