import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:quinb/game/audio.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:vibration/vibration.dart';

/// Sequence game
class SequenceGame extends Game with Audio {
  SequenceGame(int players, GamePageState gamePage, {this.useVibration = false})
      : super(players, gamePage);

  /// Vibration or Sound
  bool useVibration;

  /// Represent the sequence
  String sequenceToString(List<dynamic> sequence) => sequence.fold(
      '',
      (previousValue, element) =>
          previousValue +
          (useVibration ? (element ? '__' : '.') : (element as Animal).emoji) +
          ' ');

  @override
  Future<void> prePhase() async {
    options.clear();
    var sequenceLength = 2 + settings.difficulty.index + Random().nextInt(3);
    var sequences = <List<dynamic>>[];
    for (var i = 0;
        sequences.length < min(5, 2 + settings.difficulty.index) && i < 42;
        i++) {
      var sequence = List.generate(
          sequenceLength,
          (_) => useVibration
              ? Random().nextBool()
              : Animal.values[Random().nextInt(Animal.values.length)]);
      if (sequences.every((element) => !listEquals(element, sequence))) {
        sequences.add(sequence);
        options.add(Option(options.length, text: sequenceToString(sequence)));
      }
    }
    correctValue = Random().nextInt(options.length);
    for (final element in sequences[correctValue!]) {
      if (!isPrePhase) return;
      await (useVibration
          ? Vibration.vibrate(duration: element ? 300 : 150)
          : playSound((element as Animal).fileName));
      await Future.delayed(const Duration(seconds: 1));
    }
  }
}
