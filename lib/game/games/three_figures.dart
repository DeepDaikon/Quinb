import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/colors.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';

/// Find three identical figures
class ThreeFiguresGame extends Game {
  ThreeFiguresGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  final int time = 1000 + Game.invertedDifficultyIndex * 100;

  /// List of objects
  late List<Figure> objectList;

  /// Count occurrences
  final Map<Figure, int> count = {};

  /// Letters
  final List<String> letters = ['O', 'X', 'I'];

  /// Colors
  late List<Color> colors;

  @override
  void update() {
    colors = colorList.getRandomSublist(3).cast<Color>().toList();
    objectList = List<Figure>.generate(6, (_) {
      return Figure(letters[Random().nextInt(letters.length)],
          colors[Random().nextInt(colors.length)]);
    });
    count.clear();
    for (final o in objectList) {
      count[o] = !count.containsKey(o) ? 1 : count[o]! + 1;
    }
    correctValue = count.values.any((v) => v >= 3) ? Option.tap.value : 0;
  }

  @override
  Widget widget() => questionWidget(
        child: GridView.count(
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: 3,
          childAspectRatio: (MediaQuery.of(ui.context).size.width / 3) /
              (innerWidgetHeight / 2),
          children: List.generate(6, (index) {
            return DecoratedBox(
              decoration: BoxDecoration(
                color: objectList[index].color.withOpacity(
                    isPostPhase && count[objectList[index]]! < 3 ? 0.4 : 1),
                border: Border.all(color: Colors.white),
              ),
              child: FittedBox(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    objectList[index].text,
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
            );
          }),
        ),
      );
}

enum Shape { square, circle, triangle }

class Figure {
  Figure(this.text, this.color, {this.shape = Shape.square});
  Figure.empty() : this('', Colors.black);

  /// Text inside
  String text;

  /// Color
  Color color;

  /// Shape
  Shape shape;

  @override
  bool operator ==(other) =>
      other is Figure &&
      text == other.text &&
      color == other.color &&
      shape == other.shape;

  @override
  int get hashCode => text.hashCode ^ color.hashCode ^ shape.hashCode;
}
