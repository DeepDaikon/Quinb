import 'dart:math';

import 'package:flutter/material.dart';

/// Get the value of a random color
extension RandomColor on Random {
  int nextColorValue(int max) =>
      0xff000000 + (nextInt(max) << 16) + (nextInt(max) << 8) + nextInt(max);
}

/// Get random elements
extension Sublist on List {
  Iterable<T> getRandomSublist<T>(int count) =>
      (List<T>.from(this)..shuffle()).sublist(0, count);
}

/// Add + and - operators to Color
extension ColorOperators on Color {
  Color operator +(Color c) => Color.fromARGB(
        0xFF,
        (red + c.red) ~/ 2,
        (green + c.green) ~/ 2,
        (blue + c.blue) ~/ 2,
      );

  Color operator -(Color c) => Color.fromARGB(
        0xFF,
        red * 2 - c.red,
        green * 2 - c.green,
        blue * 2 - c.blue,
      );
}
