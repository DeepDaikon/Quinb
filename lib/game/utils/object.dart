import 'dart:math';

/// Generic object used in some games
class GameObject {
  GameObject({required this.text, this.highlighted = false})
      : quarterTurns = Random().nextInt(3);

  final String text;
  final int quarterTurns;
  final bool highlighted;

  @override
  bool operator ==(other) => other is GameObject && other.text == text;
  @override
  int get hashCode => text.hashCode;
}
