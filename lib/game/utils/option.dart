import 'package:flutter/material.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/utils/i18n.dart';

/// Game option
class Option {
  Option(this.value, {String? text, this.color}) : _text = text;

  /// Option value (used as ID)
  int value;

  /// Text displayed
  final String? _text;
  String get text => _text ?? value.toString();

  /// Option color
  Color? color;

  /// Font used to display the text
  String? get fontFamily => hasEmoji(text) ? 'emoji' : null;

  /// Single option
  static Option get tap => Option(1, text: 'Tap!'.i18n);
  static Option get yes => Option(1, text: 'Yes!'.i18n);
}

/// Convert list of int into list of Option
extension ToOptions on List<int> {
  List<Option> toOptions({bool asColors = false}) => map((e) => Option(e,
      color: asColors ? Color(e) : null, text: asColors ? ' ' : null)).toList();
}

/// Option selected by a player
class Input {
  Input(this.option, this.player);
  Option? option;
  int? player;
  bool? correct;
}
