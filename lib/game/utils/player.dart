import 'package:flutter/material.dart';
import 'package:quinb/game/utils/colors.dart';

/// Class used to store player details
class Player {
  Player({required this.id, required this.color}) : name = color.name;

  /// Player id. It equals to his index in game.playerList
  final int id;

  /// Player color
  final Color color;

  /// Player name
  final String name;

  /// Player points
  int points = 0;
}
