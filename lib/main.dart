import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:quinb/game/audio.dart';
import 'package:quinb/ui/screens/home/home_page.dart';
import 'package:quinb/ui/screens/rules/rules_page.dart';
import 'package:quinb/ui/theme.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:window_manager/window_manager.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await loadStoredData();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  if (Platform.isAndroid) {
    var androidInfo = await DeviceInfoPlugin().androidInfo;
    if (androidInfo.version.sdkInt >= 19) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    }
  } else if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    settings.enableVibrationGames = false;
    await windowManager.ensureInitialized();
    windowManager.setMinimumSize(const Size(400, 600));
    windowManager.setMaximumSize(Size.infinite);
  }
  await Localization.loadTranslations();
  await Audio.loadSoundList();
  runApp(Quinb());
}

class Quinb extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return I18n(
      initialLocale: settings.locale,
      child: MaterialApp(
        title: 'Quinb',
        theme: theme,
        home: settings.firstRun ? RulesPage(HomePage()) : HomePage(),
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: supportedLocales,
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
