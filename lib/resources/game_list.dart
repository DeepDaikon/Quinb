import 'package:quinb/resources/game_category.dart';
import 'package:quinb/resources/game_loader.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// List of games
List<GameDetails> get enabledGames => gameList
    .where((game) => !settings.disabledGames.contains(game.id.name))
    .toList();
late List<GameDetails> gameList;
void reloadGameList() {
  gameList = [
    GameDetails(
      title: 'Animal coherence'.i18n,
      subtitle: 'Tap the screen if what you see is what you hear'.i18n,
      question: 'Is it correct?'.i18n,
      id: GameId.animalCoherence,
      category: GameCategory.Sound,
      rules:
          'Tap the screen when the animal you see, the animal you read, and the animal you hear are all the same'
              .i18n,
    ),
    GameDetails(
      title: 'Animal farm'.i18n,
      subtitle: 'Tap when you hear the correct animal'.i18n,
      id: GameId.animalFarm,
      category: GameCategory.Sound,
      rules:
          'Tap the screen when you hear the correct animal during a sequence of sounds'
              .i18n,
    ),
    GameDetails(
      title: 'Ascending order'.i18n,
      subtitle: 'Tap when the list of numbers is in order'.i18n,
      question: 'Is it in ascending order?'.i18n,
      id: GameId.ascendingOrder,
      category: GameCategory.Logic,
      rules: 'Tap when the list of numbers is in ascending order'.i18n,
    ),
    GameDetails(
      title: 'Audio sequence'.i18n,
      subtitle: 'Remember the sequence you hear'.i18n,
      question: 'Which sequence was it?'.i18n,
      id: GameId.sequenceSound,
      category: GameCategory.Sound,
      rules: 'Listen to the sequence of sounds and remember it'.i18n,
    ),
    GameDetails(
      title: 'Count the animals'.i18n,
      subtitle: 'Count the animals you see'.i18n,
      id: GameId.countTheAnimals,
      category: GameCategory.Logic,
      rules: 'Count the animals you see on the screen and answer the question'
          .i18n,
    ),
    GameDetails(
      title: 'Count the lights'.i18n,
      subtitle: 'Count how many lights come on'.i18n,
      question: 'How many light have come on?'.i18n,
      id: GameId.countTheLights,
      category: GameCategory.Logic,
      rules:
          'Some lights are about to come on in rapid succession.\n\nCount how many lights come on.'
              .i18n,
    ),
    GameDetails(
      title: 'Countdown'.i18n,
      subtitle: 'Count the seconds in your head'.i18n,
      id: GameId.countdown,
      category: GameCategory.Logic,
      rules:
          'Count the seconds in your head and tap when you think the countdown is over'
              .i18n,
    ),
    GameDetails(
      title: 'Elapsed time'.i18n,
      subtitle: 'Calculate how much time has passed'.i18n,
      question: 'How much time has passed?'.i18n,
      id: GameId.elapsedTime,
      category: GameCategory.Logic,
      rules: 'Calculate how much time has passed between two times'.i18n,
    ),
    GameDetails(
      title: 'Fast finger'.i18n,
      subtitle: 'Tap as soon as you feel a vibration'.i18n,
      id: GameId.fastFinger,
      category: GameCategory.Vibration,
      rules: 'Tap as soon as you feel a vibration'.i18n,
    ),
    GameDetails(
      title: 'Fastest object'.i18n,
      subtitle: 'Find the fastest object'.i18n,
      question: 'Which one is the fastest object?'.i18n,
      id: GameId.fastestObject,
      category: GameCategory.Logic,
      rules: 'Try to figure out which one is the fastest moving object'.i18n,
    ),
    GameDetails(
      title: 'Find the cat'.i18n,
      subtitle: 'Tap when you see a cat'.i18n,
      question: 'Is there a cat?'.i18n,
      id: GameId.findTheCat,
      category: GameCategory.Logic,
      rules: 'Tap the screen when you see a cat'.i18n,
    ),
    GameDetails(
      title: 'Five objects'.i18n,
      subtitle: 'Tap when there are 5 different objects'.i18n,
      question: 'Are there 5 different objects?'.i18n,
      id: GameId.fiveObjects,
      category: GameCategory.Logic,
      rules: 'Tap when there are at least 5 different objects'.i18n,
    ),
    GameDetails(
      title: 'Hear and calculate'.i18n,
      subtitle: 'Add or subtract based on vibrations'.i18n,
      question: 'What is the result?'.i18n,
      id: GameId.vibrationMath,
      category: GameCategory.Vibration,
      rules:
          'Sum if you hear a long vibration, subtract if you hear a short one.\n\nThe amount of the sum and the subtraction will be shown during the game.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration'
              .i18n,
    ),
    GameDetails(
      title: 'Lights on'.i18n,
      subtitle: 'Count if there are more lights on than off'.i18n,
      question: 'Are there more lights on than off?'.i18n,
      id: GameId.lightsOn,
      category: GameCategory.Logic,
      rules: 'Tap the screen if there are more yellow squares than black ones'
          .i18n,
    ),
    GameDetails(
      title: 'Listen and calculate'.i18n,
      subtitle: 'Add or subtract based on what you hear'.i18n,
      question: 'What is the result?'.i18n,
      id: GameId.listenMath,
      category: GameCategory.Sound,
      rules:
          'Sum if you hear a duck, subtract if you hear a cat.\n\nThe amount of the sum and the subtraction will be shown during the game.'
              .i18n,
    ),
    GameDetails(
      title: 'Math calculation'.i18n,
      subtitle: 'Solve the equations'.i18n,
      id: GameId.mathCalculation,
      category: GameCategory.Logic,
      rules: 'Solve the equations in your head as fast as you can'.i18n,
    ),
    GameDetails(
      title: 'Memory'.i18n,
      subtitle: 'Remember the objects and their position'.i18n,
      question: 'What was in this position?'.i18n,
      id: GameId.memory,
      category: GameCategory.Logic,
      rules: 'Remember the objects and their position'.i18n,
    ),
    GameDetails(
      title: 'Mix colors'.i18n,
      subtitle: 'Mix the colors'.i18n,
      id: GameId.colorCalculation,
      category: GameCategory.Logic,
      rules: 'Try to figure out which color results from the color mixing'.i18n,
    ),
    GameDetails(
      title: 'Recognize the sound'.i18n,
      subtitle: 'Tap the screen when you hear the sound'.i18n,
      id: GameId.recognize,
      category: GameCategory.Sound,
      rules:
          'Listen to the sample sound twice here.\n\nThen tap the screen when you hear it during a sequence of sounds.'
              .i18n,
    ),
    GameDetails(
      title: 'Reflexes'.i18n,
      subtitle: 'Tap at the right moment'.i18n,
      question: 'Tap as soon as the screen becomes colored'.i18n,
      id: GameId.reflexes,
      category: GameCategory.Logic,
      rules:
          'As soon as the screen becomes colored, tap the option of the same color.\n\nKeep in mind that you have little time before the screen turns white again.'
              .i18n,
    ),
    GameDetails(
      title: 'Remember'.i18n,
      subtitle: 'Remember the colors and letters'.i18n,
      id: GameId.remember,
      category: GameCategory.Logic,
      rules:
          'Remember the sequence of colors and letters that will appear on the screen'
              .i18n,
    ),
    GameDetails(
      title: 'Remember the animals'.i18n,
      subtitle: 'Remember the animals you hear and see'.i18n,
      id: GameId.rememberSound,
      category: GameCategory.Sound,
      rules:
          'Remember the sequences of animals that you will hear, see and read.\n\nKeep in mind that what you see is not what you hear.'
              .i18n,
    ),
    GameDetails(
      title: 'Shortest vibration'.i18n,
      subtitle: 'Find the shortest vibration'.i18n,
      question: 'Which one was the shortest vibration?'.i18n,
      id: GameId.shortestVibration,
      category: GameCategory.Vibration,
      rules: 'Try to figure out which one is the shortest vibration among four'
          .i18n,
    ),
    GameDetails(
      title: 'Sound repetitions'.i18n,
      subtitle: 'Count how many times you hear the sound'.i18n,
      question: 'How many times have you heard the sound?'.i18n,
      id: GameId.soundCount,
      category: GameCategory.Sound,
      rules: 'Count how many times you hear the sound'.i18n,
    ),
    GameDetails(
      title: 'Three figures'.i18n,
      subtitle: 'Find three identical figures'.i18n,
      question: 'Are there 3 identical figures?'.i18n,
      id: GameId.threeFigures,
      category: GameCategory.Logic,
      rules:
          'Tap the screen when you see at least 3 figures that have the same color and the same letter inside'
              .i18n,
    ),
    GameDetails(
      title: 'Vibration repetitions'.i18n,
      subtitle: 'Count how many times you hear the vibration'.i18n,
      question: 'How many times have you heard the vibration?'.i18n,
      id: GameId.vibrationCount,
      category: GameCategory.Vibration,
      rules: 'Count how many times you hear the vibration'.i18n,
    ),
    GameDetails(
      title: 'Vibration sequence'.i18n,
      subtitle: 'Remember the sequence you hear'.i18n,
      question: 'Which sequence was it?'.i18n,
      id: GameId.sequenceVibration,
      category: GameCategory.Vibration,
      rules:
          'Listen to the sequence of vibrations and remember it.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration'
              .i18n,
    ),
    GameDetails(
      title: 'What color is it'.i18n,
      subtitle: 'Recognize which color is used'.i18n,
      id: GameId.whatColor,
      category: GameCategory.Logic,
      rules:
          'Answer as fast as you can which color is written or which color is used for the background or for the text'
              .i18n,
    ),
  ];
  gameList.sort((a, b) => a.title.compareTo(b.title));
}

/// Game info
class GameDetails {
  GameDetails({
    required this.title,
    required this.subtitle,
    required this.rules,
    required this.category,
    required this.id,
    this.question,
  });

  /// Game name
  String title;

  /// Game short description
  String subtitle;

  /// Game rules
  String rules;

  /// Game category
  GameCategory category;

  /// Corresponding GameId
  GameId id;

  /// Text displayed in-game (optional)
  String? question;
}
