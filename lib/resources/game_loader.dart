import 'package:quinb/game/game.dart';
import 'package:quinb/game/games/animal_coherence.dart';
import 'package:quinb/game/games/animal_farm.dart';
import 'package:quinb/game/games/ascending_order.dart';
import 'package:quinb/game/games/calculation.dart';
import 'package:quinb/game/games/count_repetitions.dart';
import 'package:quinb/game/games/count_the_animals.dart';
import 'package:quinb/game/games/count_the_lights.dart';
import 'package:quinb/game/games/countdown.dart';
import 'package:quinb/game/games/elapsed_time.dart';
import 'package:quinb/game/games/fast_finger.dart';
import 'package:quinb/game/games/fastest_object.dart';
import 'package:quinb/game/games/find_the_cat.dart';
import 'package:quinb/game/games/five_objects.dart';
import 'package:quinb/game/games/lights_on.dart';
import 'package:quinb/game/games/listen_math.dart';
import 'package:quinb/game/games/memory.dart';
import 'package:quinb/game/games/recognize.dart';
import 'package:quinb/game/games/reflexes.dart';
import 'package:quinb/game/games/remember.dart';
import 'package:quinb/game/games/sequence.dart';
import 'package:quinb/game/games/shortest_vibration.dart';
import 'package:quinb/game/games/three_figures.dart';
import 'package:quinb/game/games/what_color.dart';
import 'package:quinb/resources/game_category.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Mini-games
/// DO NOT RENAME VALUES (are used as keys in settings (disabledGames))
enum GameId {
  animalCoherence,
  animalFarm,
  ascendingOrder,
  colorCalculation,
  countTheAnimals,
  countTheLights,
  countdown,
  elapsedTime,
  fastFinger,
  fastestObject,
  findTheCat,
  fiveObjects,
  lightsOn,
  listenMath,
  mathCalculation,
  memory,
  recognize,
  reflexes,
  remember,
  rememberSound,
  sequenceSound,
  sequenceVibration,
  shortestVibration,
  soundCount,
  threeFigures,
  vibrationCount,
  vibrationMath,
  whatColor,
}

/// Load a game from an active game
Game loadGame(Game game, GameId newId) =>
    newId.load(players: game.ui.widget.playerCount, gamePageState: game.ui)
      ..playerList = game.playerList
      ..roundCount = game.roundCount;

/// Load a random game from an active game
Game nextRandomGame(Game game) =>
    loadGame(game, getRandomGame(game.ui.widget.category));

/// Return a random game optionally based on category
GameId getRandomGame(GameCategory? category) => (enabledGames
        .where((game) =>
            game.category == category ||
            (category == null &&
                (settings.enableSoundGames ||
                    game.category != GameCategory.Sound) &&
                (settings.enableVibrationGames ||
                    game.category != GameCategory.Vibration)))
        .toList()
      ..shuffle())
    .first
    .id;

/// Initialize game
extension GameLoader on GameId {
  Game load({required int players, required GamePageState gamePageState}) {
    Game game;
    switch (this) {
      case GameId.colorCalculation:
        game = CalculationGame(players, gamePageState, colorMode: true);
        break;
      case GameId.whatColor:
        game = WhatColorGame(players, gamePageState);
        break;
      case GameId.animalFarm:
        game = AnimalFarmGame(players, gamePageState);
        break;
      case GameId.countTheLights:
        game = CountTheLightsGame(players, gamePageState);
        break;
      case GameId.lightsOn:
        game = LightsOnGame(players, gamePageState);
        break;
      case GameId.listenMath:
        game = ListenMathGame(players, gamePageState);
        break;
      case GameId.mathCalculation:
        game = CalculationGame(players, gamePageState);
        break;
      case GameId.fastestObject:
        game = FastestObjectGame(players, gamePageState);
        break;
      case GameId.recognize:
        game = RecognizeGame(players, gamePageState);
        break;
      case GameId.reflexes:
        game = ReflexesGame(players, gamePageState);
        break;
      case GameId.remember:
        game = RememberGame(players, gamePageState);
        break;
      case GameId.sequenceSound:
        game = SequenceGame(players, gamePageState);
        break;
      case GameId.sequenceVibration:
        game = SequenceGame(players, gamePageState, useVibration: true);
        break;
      case GameId.shortestVibration:
        game = ShortestVibrationGame(players, gamePageState);
        break;
      case GameId.soundCount:
        game = CountGame(players, gamePageState);
        break;
      case GameId.threeFigures:
        game = ThreeFiguresGame(players, gamePageState);
        break;
      case GameId.vibrationCount:
        game = CountGame(players, gamePageState, useVibration: true);
        break;
      case GameId.vibrationMath:
        game = ListenMathGame(players, gamePageState, useVibration: true);
        break;
      case GameId.findTheCat:
        game = FindTheCatGame(players, gamePageState);
        break;
      case GameId.fiveObjects:
        game = FiveObjectsGame(players, gamePageState);
        break;
      case GameId.ascendingOrder:
        game = AscendingOrderGame(players, gamePageState);
        break;
      case GameId.fastFinger:
        game = FastFingerGame(players, gamePageState);
        break;
      case GameId.countdown:
        game = CountdownGame(players, gamePageState);
        break;
      case GameId.countTheAnimals:
        game = CountTheAnimalsGame(players, gamePageState);
        break;
      case GameId.rememberSound:
        game = RememberGame(players, gamePageState, soundMode: true);
        break;
      case GameId.animalCoherence:
        game = AnimalCoherenceGame(players, gamePageState);
        break;
      case GameId.elapsedTime:
        game = ElapsedTimeGame(players, gamePageState);
        break;
      case GameId.memory:
        game = MemoryGame(players, gamePageState);
    }
    game.id = this;
    return game;
  }
}
