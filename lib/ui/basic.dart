import 'package:flutter/material.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Linear gradient used in app
const appGradient = LinearGradient(colors: [
  Color(0xEE008080),
  Color(0xEE106075),
  Color(0xEE155070),
  Color(0xEE204569),
  Color(0xEE254069),
  Color(0xEE353769),
  Color(0xEE393469),
], begin: Alignment.topLeft, end: Alignment.bottomRight);

/// Page route
class FadeRoute extends PageRouteBuilder {
  FadeRoute(this.page)
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (BuildContext context,
                  Animation<double> animation,
                  Animation<double> secondaryAnimation,
                  Widget child) =>
              FadeTransition(opacity: animation, child: child),
        );

  @override
  final Duration transitionDuration = const Duration(milliseconds: 150);

  final Widget page;
}

/// Remove scroll glow
class NoScrollGlow extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
          BuildContext context, Widget child, ScrollableDetails details) =>
      child;
}

/// SnackBar shown in home and select page
void snackBar(GlobalKey<ScaffoldState> key,
    {bool floating = false, String? text}) {
  text ??= 'Increase system volume in order to play all the games'.i18n;
  ScaffoldMessenger.of(key.currentContext!).removeCurrentSnackBar();
  ScaffoldMessenger.of(key.currentContext!).showSnackBar(SnackBar(
    content: Text(
      text,
      style: const TextStyle(fontSize: 16),
      textAlign: TextAlign.center,
    ),
    duration: const Duration(seconds: 2),
    behavior: floating ? SnackBarBehavior.floating : SnackBarBehavior.fixed,
    elevation: 3,
    backgroundColor:
        floating ? Theme.of(key.currentContext!).primaryColor : null,
    shape: floating
        ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))
        : null,
  ));
}

String get soundToggleText => settings.enableSoundGames
    ? 'Sound-based games enabled'.i18n
    : 'Sound-based games disabled'.i18n;
String get vibrationToggleText => settings.enableVibrationGames
    ? 'Vibration-based games enabled'.i18n
    : 'Vibration-based games disabled'.i18n;
String get vibrationNotSupported =>
    'Your device does not support vibration-based games'.i18n;
