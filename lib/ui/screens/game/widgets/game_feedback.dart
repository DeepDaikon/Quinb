import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

final AudioPlayer _audioPlayer = AudioPlayer()
  ..setPlayerMode(PlayerMode.lowLatency)
  ..setReleaseMode(ReleaseMode.stop)
  ..audioCache.prefix = 'assets/audio/'
  ..audioCache.loadAll(['sfx_right.ogg', 'sfx_wrong.ogg', 'sfx_timeout.ogg']);

/// Add audio and visual feedback functionality
/// State<GamePage> MUST use [TickerProviderStateMixin]
mixin GameFeedback on State<GamePage> {
  Game? game;
  bool? get correct => game?.lastInput?.correct;
  bool get resultRotated => [1, 2].contains(game?.lastInput?.player ?? 0);

  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 200),
        vsync: this as TickerProviderStateMixin,
        value: 0);
    _animation =
        CurvedAnimation(parent: _animationController, curve: Curves.linear);
  }

  /// Answer popup controller
  void popupController({bool timeOver = false}) {
    if (settings.sfx && settings.enableSoundGames) {
      _audioPlayer.stop().then((_) => _audioPlayer.play(AssetSource(timeOver
          ? 'sfx_timeout.ogg'
          : ('sfx_${(correct ?? false) ? 'right' : 'wrong'}.ogg'))));
    }
    _animationController.forward().whenComplete(() async {
      await Future.delayed(const Duration(milliseconds: 500));
      await _animationController.reverse();
    });
  }

  /// Popup displayed after players input
  /// Green/Red/White popup (right answer/wrong answer/time over)
  Widget resultPopup() => Positioned(
        right: MediaQuery.of(context).size.width / 2 - 100,
        bottom: correct == null ? null : (resultRotated ? null : 50),
        top: correct == null
            ? MediaQuery.of(context).size.height / 2
            : (resultRotated ? 50 : null),
        child: ScaleTransition(
          scale: _animation,
          child: RotatedBox(
            quarterTurns: resultRotated ? 2 : 0,
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: (correct == null)
                      ? Colors.white
                      : (correct! ? Colors.green : Colors.red),
                  border: Border.all(color: Colors.white, width: 5)),
              width: 200,
              height: 200,
              child: Icon(
                (correct == null)
                    ? Icons.timer_off_outlined
                    : (correct! ? Icons.check_rounded : Icons.clear_rounded),
                size: 150,
                color: correct == null ? Colors.black : Colors.white,
              ),
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
