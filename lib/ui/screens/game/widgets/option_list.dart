import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/widgets/pause_menu.dart';

/// List of options
class OptionList {
  List<Widget> options(Game game, int playerId) {
    return <Widget>[
      if (game.isMainPhase || game.isPostPhase)
        for (int i = 0; i < game.options.length; i++) ...[
          Expanded(
            child: GestureDetector(
              onTapDown: (_) => game.setInput(Input(game.options[i], playerId)),
              child: DecoratedBox(
                decoration: BoxDecoration(
                    color: game.options[i].color ??
                        game.playerList[playerId].color,
                    border: game.ui.widget.playerCount > 2 && playerId <= 1
                        ? const Border(
                            right: BorderSide(color: Colors.white, width: 0.4))
                        : null),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          game.options[i].text,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontFamily: game.options[i].fontFamily,
                          ),
                        ),
                      ),
                    ),
                    if (game.isPostPhase)
                      Positioned.fill(
                        right: 10,
                        child: Align(
                          alignment: Alignment.centerRight,
                          child:
                              correctnessIcon(game, game.options[i], playerId),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
          if (i != game.options.length - 1)
            Divider(height: 0.1, color: Colors.white.withOpacity(0.01))
        ]
      else if ((game.isOver && playerId == game.ranking.first.id) ||
          (!game.isOver &&
              game.ui.pauseMenu != PauseMenu.none &&
              playerId == game.ui.pausedBy))
        ...menuOptions(game)
    ];
  }

  /// Icon showing whether the option was correct or not
  Widget correctnessIcon(Game game, Option option, int playerId) {
    if (game.lastInput?.player == playerId &&
        game.lastInput?.option == option &&
        !(game.ui.correct ?? true)) {
      return const Icon(Icons.clear_rounded, color: Colors.white);
    } else if (game.lastInput?.player == playerId &&
        game.ui.correct != null &&
        game.correctValue == option.value) {
      return const Icon(Icons.done_rounded, color: Colors.white);
    } else if ((game.lastInput == null ||
            (game.lastInput?.player != playerId && game.ui.correct != null)) &&
        game.correctValue == option.value) {
      return const Icon(Icons.arrow_left_rounded, color: Colors.white);
    }
    return Container();
  }
}
