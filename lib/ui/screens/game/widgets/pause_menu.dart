import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/utils/i18n.dart';

/// None: the menu is closed
/// Pause: the menu is showing 'resume', 'restart', 'exit'
/// Restart: the menu is showing 'cancel', 'restart'
/// Exit: the menu is showing 'cancel', 'exit' or 'restart', 'exit' if isOver
enum PauseMenu { none, pause, restart, exit }

/// Options shown in the menu
List<Widget> menuOptions(Game game) {
  List<Option> options;
  if (game.ui.pauseMenu == PauseMenu.pause) {
    options = [
      Option(0, text: 'Resume'.i18n),
      Option(1, text: 'Restart'.i18n),
      Option(2, text: 'Exit'.i18n),
    ];
  } else if (game.ui.pauseMenu == PauseMenu.restart) {
    options = [Option(3, text: 'Cancel'.i18n), Option(4, text: 'Restart'.i18n)];
  } else if (game.isOver) {
    options = [Option(4, text: 'Restart'.i18n), Option(5, text: 'Exit'.i18n)];
  } else {
    options = [Option(3, text: 'Cancel'.i18n), Option(5, text: 'Exit'.i18n)];
  }
  return [
    for (int i = 0; i < options.length; i++) ...[
      Expanded(
        child: InkWell(
          onTap: () {
            switch (options[i].value) {
              case 0:
                game.pause();
                break;
              case 1:
                game.ui.pauseMenu = PauseMenu.restart;
                game.ui.refresh();
                break;
              case 2:
                game.ui.pauseMenu = PauseMenu.exit;
                game.ui.refresh();
                break;
              case 3:
                game.ui.pauseMenu = PauseMenu.pause;
                game.ui.refresh();
                break;
              case 4:
                game.ui.pauseMenu = PauseMenu.none;
                game.restart();
                break;
              case 5:
                Navigator.pop(game.ui.context);
                break;
            }
          },
          child: Container(
            color: game.isOver
                ? game.ranking.first.color
                : Theme.of(game.ui.context).primaryColor,
            child: Center(
              child: Text(
                options[i].text,
                style: const TextStyle(fontSize: 22, color: Colors.white),
              ),
            ),
          ),
        ),
      ),
      if (i != options.length - 1)
        Divider(height: 0.1, color: Colors.white.withOpacity(0.01))
    ]
  ];
}
