import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Timer widget
class TimerWidget extends StatefulWidget {
  TimerWidget(this.game);
  final Game game;

  @override
  State<TimerWidget> createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  late AnimationController controller;

  bool get changeTurn => widget.game.time == 0;
  int get timeAvailable =>
      changeTurn ? settings.timeAvailable * 1000 : widget.game.time;

  /// Max height
  late double height;

  @override
  void initState() {
    height = widget.game.innerWidgetHeight - 20;
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: timeAvailable),
      upperBound: height,
    )..forward(
        from:
            height * (widget.game.elapsedTime.inMilliseconds / timeAvailable));
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (changeTurn) {
          widget.game.setInput();
        } else {
          widget.game.elapsedTime = Duration.zero;
          widget.game.updater?.call();
          controller
            ..reset()
            ..forward();
        }
      }
    });
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.game.hideTimer) return Container();
    final Widget timerWidget = _widget();
    final double halfHeight = widget.game.multiplayer
        ? (MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top -
                    MediaQuery.of(context).padding.bottom) /
                2 -
            height / 2
        : widget.game.innerWidgetHeight / 2 - height / 2;
    return Stack(
      children: <Widget>[
        if (widget.game.multiplayer)
          Positioned(top: halfHeight, left: 2, child: timerWidget),
        Positioned(top: halfHeight, right: 2, child: timerWidget),
      ],
    );
  }

  Widget _widget() => SizedBox(
        height: height,
        child: Center(
          child: AnimatedBuilder(
            animation: CurvedAnimation(
                parent: controller, curve: Curves.fastLinearToSlowEaseIn),
            builder: (context, child) {
              return Container(
                width: 3,
                height: controller.upperBound - controller.value,
                decoration: BoxDecoration(
                  color: widget.game.backgroundColor.computeLuminance() > 0.5
                      ? Colors.black54
                      : Colors.white54,
                  borderRadius: BorderRadius.circular(30),
                ),
              );
            },
          ),
        ),
      );

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      controller.stop();
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    if (controller.lastElapsedDuration != null) {
      widget.game.elapsedTime += controller.lastElapsedDuration!;
    } else {
      widget.game.elapsedTime = Duration.zero;
    }
    controller.dispose();
    super.dispose();
  }
}
