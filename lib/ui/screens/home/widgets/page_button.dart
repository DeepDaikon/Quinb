import 'package:flutter/material.dart';

class PageButton extends StatelessWidget {
  PageButton({
    required this.title,
    required this.onPressed,
  });

  final String title;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.2,
      height: MediaQuery.of(context).size.height / 10,
      constraints:
          const BoxConstraints(maxWidth: 275, maxHeight: 50, minHeight: 25),
      margin: const EdgeInsets.only(bottom: 25),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.black45),
          borderRadius: const BorderRadius.all(Radius.circular(30))),
      child: TextButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)))),
        onPressed: onPressed,
        child: Text(title,
            maxLines: 1,
            style: const TextStyle(color: Colors.black, fontSize: 21)),
      ),
    );
  }
}
