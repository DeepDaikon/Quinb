import 'package:flutter/material.dart';
import 'package:quinb/ui/screens/rules/utils/rule.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:vibration/vibration.dart';

/// Global rules
List<Rule> rules() => [
      Rule(
        title: 'Q u i n b',
        subtitle:
            'Quinb is a multiplayer reaction game.\nTrain your mind and your reflexes while having fun!\n\nSwipe to right to learn how to play.'
                .i18n,
        image: 'assets/graphics/icon_black.png',
      ),
      Rule(
        title: 'Games'.i18n,
        subtitle:
            'Quinb contains many minigames based on 3 different categories:\nlogic, audio, vibration.'
                .i18n,
        icon: Icons.games_rounded,
      ),
      Rule(
        title: 'Points'.i18n,
        subtitle:
            'In every game you have to answer questions as fast as you can.\nIf the answer is correct you score a point, otherwise you lose one.\n'
                .i18n,
        icon: Icons.exposure_plus_1_rounded,
      ),
      Rule(
        title: 'Goal'.i18n,
        subtitle:
            'Each match consists of a succession of different minigames.\nThe goal is to score 7 points before your opponents.'
                .i18n,
        icon: Icons.beenhere_rounded,
      ),
      Rule(
        title: 'Multiplayer'.i18n,
        subtitle:
            'Quinb can be played by 4 players on the same device, but you can also play alone if you want.'
                .i18n,
        icon: Icons.people_rounded,
      ),
      if (settings.enableVibrationGames)
        Rule(
            title: 'Vibration'.i18n,
            subtitle:
                '''Some minigames require your device to vibrate.\n\nIf you don't feel vibrations, enable "Vibration and haptics" from system settings or disable vibration games from the app settings.'''
                    .i18n,
            icon: Icons.vibration_rounded,
            callback: () {
              Vibration.vibrate(duration: 100);
            }),
      Rule(
        title: "That's all".i18n,
        subtitle:
            'You will learn the rules of each minigame by playing.\n\nKeep in mind that this game is way more fun if played with friends!'
                .i18n,
        icon: Icons.done_rounded,
        iconSize: 210,
      ),
    ];
