import 'package:flutter/material.dart';

/// Game rule
class Rule {
  Rule({
    required this.title,
    required this.subtitle,
    this.image,
    this.icon,
    this.iconSize = 150,
    this.callback,
  });

  /// Rule title
  final String title;

  /// Rule description
  final String subtitle;

  /// Rule image if white theme
  final String? image;

  /// Rule icon (instead of image)
  final IconData? icon;
  final double iconSize;

  /// Callback
  final VoidCallback? callback;
}
