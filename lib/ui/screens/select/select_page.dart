import 'dart:io';

import 'package:flutter/material.dart';
import 'package:quinb/resources/game_category.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/ui/screens/rules/rules_page.dart';
import 'package:quinb/ui/screens/select/utils/change_page.dart';
import 'package:quinb/ui/screens/select/widgets/bottom_bar.dart';
import 'package:quinb/ui/screens/select/widgets/fab.dart';
import 'package:quinb/ui/screens/settings/settings_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

class SelectPage extends StatefulWidget {
  @override
  State<SelectPage> createState() => _SelectPageState();
}

class _SelectPageState extends State<SelectPage> {
  /// Category shown (0 -> All)
  int _selectedCategory = 0;

  /// List of games in the selected category
  List<GameDetails> get _gameList => enabledGames
      .where((game) =>
          _selectedCategory == 0 ||
          game.category == GameCategory.values[_selectedCategory - 1])
      .toList();

  /// Key of the scaffold of this page
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  /// Top bar with title and quick options
  PreferredSizeWidget appBar() {
    return AppBar(
      elevation: 0,
      title: Text('Games'.i18n.toUpperCase(),
          style: TextStyle(color: Theme.of(context).primaryColor)),
      centerTitle: true,
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(52),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom:
                  BorderSide(color: Theme.of(context).primaryColor, width: 3),
            ),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(settings.enableSoundGames
                    ? Icons.volume_up_rounded
                    : Icons.volume_off_rounded),
                tooltip: 'Sound games'.i18n,
                onPressed: () {
                  setState(() {
                    _selectedCategory = 0;
                    settings.enableSoundGames = !settings.enableSoundGames;
                    saveSettings();
                    snackBar(_scaffoldKey,
                        floating: true, text: soundToggleText);
                  });
                },
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(settings.enableVibrationGames
                    ? Icons.vibration_rounded
                    : Icons.mobile_off_rounded),
                tooltip: 'Vibration games'.i18n,
                onPressed: Platform.isAndroid
                    ? () {
                        setState(() {
                          _selectedCategory = 0;
                          settings.enableVibrationGames =
                              !settings.enableVibrationGames;
                          saveSettings();
                          snackBar(
                            _scaffoldKey,
                            floating: true,
                            text: vibrationToggleText,
                          );
                        });
                      }
                    : () => snackBar(_scaffoldKey,
                        floating: true, text: vibrationNotSupported),
              ),
              Expanded(
                child: Center(
                  child: FittedBox(
                    child: DropdownButton<int>(
                      value: settings.players,
                      underline: Container(),
                      icon: Container(),
                      focusColor: Colors.transparent,
                      elevation: 0,
                      onChanged: (newValue) {
                        setState(() {
                          settings.players = newValue!;
                          saveSettings();
                        });
                      },
                      items: [1, 2, 3, 4]
                          .map<DropdownMenuItem<int>>(
                            (int value) => DropdownMenuItem<int>(
                              value: value,
                              child: Text(
                                '$value ' +
                                    (value > 1
                                        ? 'players'.i18n
                                        : 'player'.i18n),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: const Icon(Icons.help_outline_rounded),
                tooltip: 'How to play'.i18n,
                onPressed: () {
                  Navigator.push(context, FadeRoute(RulesPage()));
                },
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: const Icon(Icons.settings_rounded),
                tooltip: 'Settings'.i18n,
                onPressed: () {
                  Navigator.push(context, FadeRoute(SettingsPage()))
                      .then((_) => setState(() {}));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: appBar(),
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onHorizontalDragEnd: (DragEndDetails details) {
          setState(() => _selectedCategory = changePage(
              _selectedCategory, details.velocity.pixelsPerSecond.dx,
              isRtl: Directionality.of(context) == TextDirection.rtl));
        },
        child: _gameList.isEmpty
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 25),
                    child: Icon(Icons.visibility_off_rounded, size: 50),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: Text(
                      'You have disabled all games in this category'.i18n,
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontSize: 20),
                    ),
                  ),
                ],
              )
            : ListView.separated(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 35),
                itemCount: _gameList.length,
                itemBuilder: (BuildContext context, int index) => ((settings
                                .enableSoundGames ||
                            _gameList[index].category != GameCategory.Sound) &&
                        (settings.enableVibrationGames ||
                            _gameList[index].category !=
                                GameCategory.Vibration))
                    ? ListTile(
                        title: Text(
                          _gameList[index].title,
                          style: const TextStyle(fontSize: 20),
                        ),
                        subtitle: Text(_gameList[index].subtitle),
                        leading: Icon(_gameList[index].category.icon),
                        isThreeLine: true,
                        onTap: () {
                          Navigator.push(
                            context,
                            FadeRoute(GamePage(
                                playerCount: settings.players,
                                id: _gameList[index].id)),
                          );
                        },
                      )
                    : Container(),
                separatorBuilder: (BuildContext context, int index) => (settings
                                .enableSoundGames ||
                            _gameList[index].category != GameCategory.Sound) &&
                        (settings.enableVibrationGames ||
                            _gameList[index].category != GameCategory.Vibration)
                    ? Divider(height: 1, color: Colors.grey.withOpacity(0.4))
                    : Container()),
      ),
      bottomNavigationBar: bottomBar(context, _selectedCategory, (i) {
        setState(() {
          _selectedCategory = i + 1;
        });
      }),
      floatingActionButton: Fab(_selectedCategory),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}
