import 'dart:math';

import 'package:quinb/resources/game_category.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Calculate and return new page index
int changePage(int currentIndex, double dx, {required bool isRtl}) {
  var rtl = isRtl ? -1 : 1;
  currentIndex += (dx * rtl > 0) ? -1 : 1;
  if (!settings.enableSoundGames &&
      currentIndex == GameCategory.values.indexOf(GameCategory.Sound) + 1) {
    currentIndex += (dx * rtl > 0 || !settings.enableVibrationGames) ? -1 : 1;
  }
  if (!settings.enableVibrationGames &&
      currentIndex == GameCategory.values.indexOf(GameCategory.Vibration) + 1) {
    currentIndex -= 1;
  }
  return min(max(currentIndex, 0), 3);
}
