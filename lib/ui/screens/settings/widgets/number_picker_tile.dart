import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Tile with horizontal number picker
Widget numberPickerTile(
  BuildContext context,
  StateSetter setState,
  Map<String, bool> expanded, {
  required String itemId,
  required String title,
  required String subtitle,
  required Function getValue,
  required int minValue,
  required int maxValue,
  required Function callback,
}) {
  return ExpansionTile(
    title: Text(title,
        style: TextStyle(
            color: Theme.of(context).listTileTheme.textColor, fontSize: 20)),
    subtitle: Text(subtitle,
        style: TextStyle(color: Theme.of(context).textTheme.caption!.color)),
    trailing: expanded[itemId] ?? false
        ? const SizedBox()
        : Padding(
            padding: const EdgeInsets.only(right: 16),
            child: Text(
              getValue().toString(),
              style: const TextStyle(fontSize: 16),
            ),
          ),
    onExpansionChanged: (enabled) => setState(() => expanded[itemId] = enabled),
    children: <Widget>[
      ScrollConfiguration(
        behavior: NoScrollGlow(),
        child: NumberPicker(
          value: getValue(),
          minValue: minValue,
          maxValue: maxValue,
          axis: Axis.horizontal,
          selectedTextStyle:
              TextStyle(color: Theme.of(context).primaryColor, fontSize: 26),
          onChanged: (newValue) => setState(() {
            callback(newValue);
            saveSettings();
          }),
        ),
      ),
    ],
  );
}
