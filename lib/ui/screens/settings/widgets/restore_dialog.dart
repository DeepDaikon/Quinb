import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Dialog used to restore default settings
Future restoreSettingsDialog(BuildContext context, StateSetter setState) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Restore default settings?'.i18n),
        content: Text(
            'Are you sure you want to delete your settings and restore default ones?'
                .i18n),
        actions: <Widget>[
          TextButton(
            child: Text('Restore'.i18n,
                style: const TextStyle(color: Colors.black)),
            onPressed: () {
              restoreSettings();
              I18n.of(context).locale = settings.locale;
              reloadGameList();
              saveSettings();
              setState(() {});
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            onPressed: Navigator.of(context).pop,
            child: Text('Cancel'.i18n,
                style: const TextStyle(color: Colors.black)),
          ),
        ],
      );
    },
  );
}
