import 'dart:convert';

import 'package:quinb/utils/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Game settings and preferences
late Settings settings;
late SharedPreferences _prefs;

/// Import data from shared preferences
Future<void> loadStoredData() async {
  _prefs = await SharedPreferences.getInstance();
  settings = Settings(jsonDecode(_prefs.getString('settings_V2') ?? '{}'));
  if (_prefs.containsKey('settings')) {
    settings.firstRun = false;
    saveSettings();
    _prefs.remove('settings');
  }
}

/// Save app settings in shared preferences
void saveSettings() {
  _prefs.setString('settings_V2', jsonEncode(settings.toJson()));
}

/// Restore default settings
void restoreSettings() {
  settings = Settings({
    'firstRun': false,
    'players': settings.players,
    'enableSoundGames': settings.enableSoundGames,
    'enableVibrationGames': settings.enableVibrationGames,
  });
}
