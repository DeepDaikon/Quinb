import 'dart:io';
import 'dart:ui';

import 'package:quinb/utils/i18n.dart';

/// Game difficulty
// i18n: 'Easiest'.i18n 'Easy'.i18n 'Medium'.i18n 'Hard'.i18n 'Hardest'.i18n
// ignore: constant_identifier_names
enum Difficulty { Easiest, Easy, Medium, Hard, Hardest }

/// App settings
class Settings {
  Settings(Map<String, dynamic> json)
      : difficulty = Difficulty.values[
            json['difficulty'] ?? Difficulty.values.indexOf(defaultDifficulty)],
        requiredPoints = json['requiredPoints'] ?? defaultRequiredPoints,
        timeAvailable = json['timeAvailable'] ?? defaultTimeAvailable,
        maxRounds = json['maxRounds'] ?? defaultMaxRounds,
        players = json['players'] ?? defaultPlayers,
        waitingTime = json['waitingTime'] ?? defaultWaitingTime,
        enableVolumeOnStart = json['enableVolumeOnStart'],
        enableSoundGames = json['enableSoundGames'] ?? defaultEnableSoundGames,
        enableVibrationGames =
            json['enableVibrationGames'] ?? defaultEnableVibrationGames,
        disabledGames = json['disabledGames'] ?? [],
        firstRun = json['firstRun'] ?? true,
        sfx = json['sfx'] ?? defaultSfx,
        _languageCode = json['languageCode'],
        use24h = json['use24h'] ?? defaultUse24h,
        retryUntilCorrect =
            json['retryUntilCorrect'] ?? defaultRetryUntilCorrect,
        showActionsOnLoading =
            json['showActionsOnLoading'] ?? defaultShowActionsOnLoading;

  Map<String, dynamic> toJson() => {
        'difficulty': difficulty.index,
        'requiredPoints': requiredPoints,
        'timeAvailable': timeAvailable,
        'maxRounds': maxRounds,
        'players': players,
        'waitingTime': waitingTime,
        'enableVolumeOnStart': enableVolumeOnStart,
        'enableSoundGames': enableSoundGames,
        'enableVibrationGames': enableVibrationGames,
        'disabledGames': disabledGames,
        'firstRun': firstRun,
        'sfx': sfx,
        'languageCode': _languageCode,
        'use24h': use24h,
        'retryUntilCorrect': retryUntilCorrect,
        'showActionsOnLoading': showActionsOnLoading,
      };

  /// Game difficulty
  Difficulty difficulty;
  static const Difficulty defaultDifficulty = Difficulty.Medium;

  /// Points required to win
  int requiredPoints;
  static const int defaultRequiredPoints = 7;

  /// Time available in-game
  int timeAvailable;
  static const int defaultTimeAvailable = 7;

  /// Maximum number of rounds
  int maxRounds;
  static const int defaultMaxRounds = 40;

  /// Number of players
  int players;
  static const int defaultPlayers = 2;

  /// Waiting time before a match
  int waitingTime;
  static const int defaultWaitingTime = 4;

  /// True if the app should force volume up when the app starts
  bool? enableVolumeOnStart;

  /// True if sound-based game should be shown
  bool enableSoundGames;
  static const bool defaultEnableSoundGames = true;

  /// True if vibration-based game should be shown
  bool enableVibrationGames;
  static const bool defaultEnableVibrationGames = true;

  /// Disabled games
  List<dynamic> disabledGames;

  /// Sound effects
  bool sfx;
  static const bool defaultSfx = true;

  /// True if it is the first run of the app
  bool firstRun;

  /// Display time in the 24-hour time notation
  bool use24h;
  static const bool defaultUse24h = true;

  /// Retry a game until a correct answer
  bool retryUntilCorrect;
  static const bool defaultRetryUntilCorrect = false;

  /// Show skip button and play button in the loading page
  bool showActionsOnLoading;
  static const bool defaultShowActionsOnLoading = true;

  /// App language
  String? _languageCode;
  bool get useSystemLanguage => _languageCode == null;
  String get _currentLanguageCode =>
      useSystemLanguage ? Platform.localeName : _languageCode!;
  Locale get locale =>
      enabledLocales.contains(Locale(_currentLanguageCode.split('_').first)) ||
              !useSystemLanguage
          ? Locale(_currentLanguageCode.split('_').first)
          : const Locale('en');
  set locale(Locale? locale) => _languageCode = locale?.toString();
}
