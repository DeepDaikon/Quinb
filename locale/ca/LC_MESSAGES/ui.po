# Translation for Quinb
# Copyright (C) 2020, 2021 DeepDaikon
# This file is distributed under the same license as Quinb.
# omarmaciasmolina <omarmaciasmolina@gmail.com>, 2022.
msgid ""
msgstr "Project-Id-Version: \nPOT-Creation-Date: \nPO-Revision-Date: 2022-04-27 14:11+0000\nLast-Translator: omarmaciasmolina <omarmaciasmolina@gmail.com>\nLanguage-Team: Catalan <https://translate.deepdaikon.xyz/projects/quinb/ui/ca/>\nLanguage: ca\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=n != 1;\nX-Generator: Weblate 4.4.2\n"

#: lib/ui/basic.dart:
msgid "Increase system volume in order to play all the games"
msgstr "Cal pujar el volum del so per a jugar a tota el jocs"

#: lib/ui/basic.dart:
msgid "Sound-based games enabled"
msgstr "Jocs basats en el so avtivats"

#: lib/ui/basic.dart:
msgid "Sound-based games disabled"
msgstr "Jocs basats en el so desactivats"

#: lib/ui/basic.dart:
msgid "Vibration-based games enabled"
msgstr "Jocs basats en la vibració activats"

#: lib/ui/basic.dart:
msgid "Vibration-based games disabled"
msgstr "Jocs basats en la vibració desactivats"

#: lib/ui/basic.dart:
msgid "Your device does not support vibration-based games"
msgstr "Aquest dispositiu no suporta jocs basats en la vibració"

#: lib/ui/screens/home/home_page.dart:
msgid "Play"
msgstr "Jugar"

#: lib/ui/screens/home/home_page.dart:
msgid "Select game"
msgstr "Seleccionar joc"

#: lib/ui/screens/home/home_page.dart:
msgid "Settings"
msgstr "Paràmetres"

#: lib/ui/screens/home/home_page.dart:
msgid "Sound games"
msgstr "Jocs sonors"

#: lib/ui/screens/home/home_page.dart:
msgid "Vibration games"
msgstr "Jocs de vibració"

#: lib/ui/screens/home/home_page.dart:
msgid "players"
msgstr "Jugadors"

#: lib/ui/screens/home/home_page.dart:
msgid "player"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "How to play"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Info"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Enable volume?"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "It looks like you have audio muted.\n\nYou should enable it in order to play sound-based games."
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Remember choice"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Cancel"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Disable audio games"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Enable volume"
msgstr ""

#: lib/ui/screens/game/widgets/play_timer_buttons.dart:
msgid "Pause"
msgstr ""

#: lib/ui/screens/game/widgets/play_timer_buttons.dart:
msgid "Resume"
msgstr ""

#: lib/ui/screens/game/widgets/player_zone.dart:
msgid "points"
msgstr ""

#: lib/ui/screens/game/widgets/pause_menu.dart:
msgid "Restart"
msgstr ""

#: lib/ui/screens/game/widgets/pause_menu.dart:
msgid "Exit"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Quinb is a multiplayer reaction game.\nTrain your mind and your reflexes while having fun!\n\nSwipe to right to learn how to play."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Games"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Quinb contains many minigames based on 3 different categories:\nlogic, audio, vibration."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Points"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "In every game you have to answer questions as fast as you can.\nIf the answer is correct you score a point, otherwise you lose one.\n"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Goal"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Each match consists of a succession of different minigames.\nThe goal is to score 7 points before your opponents."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Multiplayer"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Quinb can be played by 4 players on the same device, but you can also play alone if you want."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Vibration"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "Some minigames require your device to vibrate.\n\nIf you don't feel vibrations, enable "Vibration and haptics" from system settings or disable vibration games from the app settings."
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "That's all"
msgstr ""

#: lib/ui/screens/rules/resources/rules.dart:
msgid "You will learn the rules of each minigame by playing.\n\nKeep in mind that this game is way more fun if played with friends!"
msgstr ""

#: lib/ui/screens/rules/rules_page.dart:
msgid "Close"
msgstr ""

#: lib/ui/screens/rules/rules_page.dart:
msgid "Back"
msgstr ""

#: lib/ui/screens/rules/rules_page.dart:
msgid "OK"
msgstr ""

#: lib/ui/screens/rules/rules_page.dart:
msgid "Next"
msgstr ""

#: lib/ui/screens/select/widgets/fab.dart:
msgid "Random"
msgstr ""

#: lib/ui/screens/select/widgets/bottom_bar.dart:
msgid "All"
msgstr ""

#: lib/ui/screens/select/select_page.dart:
msgid "You have disabled all games in this category"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Restore"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Difficulty"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Difficulty of the games"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Points required"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Points required to win"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Round duration"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Time available each round (in seconds)"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Rounds"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Maximum number of rounds"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Waiting time"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Waiting time before a match (in seconds)"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Sound effects"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Play sound effects during the game"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "24-hour clock"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Use the 24-hour time notation in games"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Show actions on loading"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Show skip button and play button in the loading page"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Retry until correct"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Try again mini-games until someone gives a correct answer"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Disabled games"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Select the games you don't want to play anymore"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Language"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "App language"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "System default"
msgstr ""

#: lib/ui/screens/settings/widgets/disable_game_dialog.dart:
msgid "Ok"
msgstr ""

#: lib/ui/screens/settings/widgets/restore_dialog.dart:
msgid "Restore default settings?"
msgstr ""

#: lib/ui/screens/settings/widgets/restore_dialog.dart:
msgid "Are you sure you want to delete your settings and restore default ones?"
msgstr ""

#: lib/ui/screens/info/widgets/license_dialog.dart:
msgid "Third Party Licenses"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "By %s"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "App developed by %s"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Version:"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "App version"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Donate"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Support the development"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Translate"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Translate in your language"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Send email"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Ask for something or request a new feature"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Report bugs"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Report bugs or request new feature"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "View source code"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Look at the source code"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "View License"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Read software license"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Read third party notices"
msgstr ""

#: lib/utils/settings.dart:
msgid "Easiest"
msgstr ""

#: lib/utils/settings.dart:
msgid "Easy"
msgstr ""

#: lib/utils/settings.dart:
msgid "Medium"
msgstr ""

#: lib/utils/settings.dart:
msgid "Hard"
msgstr ""

#: lib/utils/settings.dart:
msgid "Hardest"
msgstr ""
